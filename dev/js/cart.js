(function($){

if($('#cesta_produtos').length){
  var $cesta = $('#cesta_produtos');

  $cesta.append('<div class="row"><div class="col-lg-8 col-md-7 col-xs-12 tabela-carrinho"></div><div class="col-lg-4 col-md-5 col-xs-12"><div class="info-carrinho"><h3>Finalizar Compra</h3></div></div></div>');

  $('#tabela_carrinho').appendTo('.tabela-carrinho');
  $('.caixa-forma-frete, .caixa-total').appendTo('.info-carrinho');
  $('.caixa-botoes').insertAfter('.info-carrinho');
  $('.caixa-forma-frete').prepend('<div class="caixa-frete-nova"><h4>Calcule aqui seu frete</h4></div>');
  $('#calculoFrete, .carFretePara').appendTo('.caixa-frete-nova');
  $('#cep1').attr('placeholder', 'Digite seu CEP');

  if($('.caixa-forma-frete #frete7').length){
    $('.caixa-total > .container2').clone().addClass('caixa-total-frete').prependTo('.caixa-total');
    $('.caixa-total-frete .tit-total').text('Valor do Frete');
    $('.caixa-total-frete .tablePage h3').html($('.caixa-forma-frete .tablePage h3').html());
  }

  if($('.caixa-cupom').length){
    $('#cupon').attr('placeholder', 'Ex: Desconto10');
    $('<div class="caixa-cupom-nova"><h4>Cupom de desconto</h4></div>').insertAfter('.caixa-frete-nova');

    $('.caixa-total > .container2:last-child').clone().addClass('caixa-total-cupom').insertAfter('.caixa-total-frete');
    $('.caixa-total-cupom .tit-total').text('Valor de Desconto');
    $('.caixa-total-cupom .tablePage h3').html($('.caixa-cupom').next().html());

    $('.caixa-cupom').parents('tr').hide();
    $('.caixa-cupom').appendTo('.caixa-cupom-nova');
  }
}

})(jQuery);