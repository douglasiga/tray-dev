(function($){

    function carousel(){
        if($(".carousel-product").length){
        $(".carousel-product").owlCarousel({
            margin: 0,
            dots: false,
            nav: true,
            lazyLoad:true,
            navText: ['<i class="ico ico-arrow-left"></i>', '<i class="ico ico-arrow-right"></i>'],
            responsive:{
                0: {
                    items: 1
                },
                610: {
                    items: 2
                },
                992: {
                    items: 4
                }
            }
        });
        }
        
        if($(".carousel-gallery").length){
        $(".carousel-gallery").owlCarousel({
            margin: 10,
            dots: false,
            nav: true,
            loop: false,
            navText: ['<i class="ico ico-arrow-left"></i>', '<i class="ico ico-arrow-right"></i>'],
            responsive:{
                0: {
                    items: 3
                }
            }
        });
        }
    }

    function flexslider(){
        if($('.fullbanner').length){
            $('.flexslider').flexslider({
                animation: "fade",
                controlNav: true,
                animationLoop: false,
                directionNav: false,
                slideshow: true
            });
        }
    }
    
    $(document).ready( function(){

        var html = $('html');

        // triggers to open and close menu mobile
        $('.trigger-menu, .menu-mobile-backdrop').click(function(){
           html.addClass('menu-open'); 
        });
        
        $('.menu-mobile-backdrop,.backdrop-icon').click(function(){
           html.removeClass('menu-open'); 
        });
        
        // cart support class
        if($('.caixa-cupom').length){
            $('.caixa-cupom').parents('tr').addClass('cupom-wrapper');
        }
        
        if($('#calculoFrete').length){
            $('#calculoFrete').parents('tr').addClass('frete-wrapper');
        }

        carousel();
        flexslider();
    
    });

    $(window).scroll(function () {
        var $scroll = jQuery(this).scrollTop(),
            $fixed = jQuery('.js-fixed');
        if ($scroll < 230) {
            $fixed.css('top', '-135px');
        }else{
            $fixed.css('top', '0px');
        }
    });
	
    // 	gifts list fix
    if($(".page-lista, .page-print_lista").length){
        $(".lista-produtos").wrapAll("<div class='wrap-gifts'></div>");
    }
    
})(jQuery);