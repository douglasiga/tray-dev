function process(quant){
    var value = parseInt(document.getElementById("quant").value);
    if(value == 1 && quant == '-1'){
        quant = 0;
    }
    value+=quant;
    document.getElementById("quant").value = value;
}

(function($){

    if($('.product-variants').length){
        $('.product-variants').prependTo('.productAdditionalInformation > #menuVars');

        $('.productAdditionalInformation > #menuVars .varTit').each(function(){
           $(this).prependTo($(this).next());
        });

        $('.info-escreva-o-nomepalavra-desejada + input').attr('placeholder', 'Ex: Caio');
    }

    if($('.produto-calcular-frete').length){
        $('.produto-formas-pagamento').appendTo($('.frete-calcular-produto'));
        $('#cep1').attr('placeholder', 'Digite seu CEP');
    }

    if($('#economize').length){
        $('#economize').insertAfter('#info_preco');
    }

    if($('.fotosCompreJunto').length){
        var storeid = $('html').attr('data-store');
        $('.fotosCompreJunto .produto img').each(function(){
            var ref = $(this).attr('src'),
                rep = ref.replace(storeid+'/90_', storeid+'/180_');

            $(this).attr('src', rep);
        });
    }

    if($('.fancybox-gallery').length){
        $('.fancybox-gallery').fancybox({
            padding: 0
        });
    }

})(jQuery);