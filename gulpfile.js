/**
 * Gulpfile
 * @author Douglas Iga <doug.ishibashi@gmail.com>
 */

var gulp = require("gulp"),
  sass = require("gulp-sass"),
  concat = require("gulp-concat"),
  minifyCSS = require("gulp-minify-css"),
  uglify = require("gulp-uglify"),
  path = require("path"),
  combineMq = require('gulp-combine-mq'),
  spritesmith = require('gulp.spritesmith');

var SASSPATH, CSSPATH, JSPATH;

gulp.task('sass', function(){
  gulp.src(SASSPATH + '/theme.min.scss')
    .pipe(sass({errLogToConsole: true}))
    .pipe(combineMq())
    .pipe(concat('theme.min.css'))
    .pipe(minifyCSS())
    .pipe(gulp.dest(CSSPATH));

  gulp.src(SASSPATH + '/theme.min.scss')
    .pipe(sass({errLogToConsole: true}))
    .pipe(combineMq())
    .pipe(concat('theme.css'))
    .pipe(gulp.dest(CSSPATH));
});

gulp.task('js', function(){
  gulp.src(JSSPATH + "/*.js")
    .pipe(concat("theme.min.js"))
    .pipe(uglify({"compress": false}))
    .pipe(gulp.dest(JSPATH));
});

// src plugins
var pluginsFile = path.resolve(process.cwd(), "../plugins");

gulp.task('plugins', function(){
  gulp.src(pluginsFile + "/*.js")
    .pipe(concat("plugins.min.js"))
    .pipe(uglify({"compress": false}))
    .pipe(gulp.dest(pluginsFile + "/dist/"));
});

// watchers
var cssWatcher = gulp.watch('**/*.scss', ['sass']);
cssWatcher.on("change", function(event) {
  SASSPATH = 'dev/sass';
  CSSPATH  = 'css/';
});

var jsWatcher = gulp.watch('**/dev/js/*.js', ['js']);
jsWatcher.on("change", function(event) {
  JSSPATH = 'dev/js';
  JSPATH = 'js/';
});

/* Images Sprites */
gulp.task('sprite', function () {
    var spriteData = gulp.src('img/sprites/*.png')
        .pipe(spritesmith({
            /* this whole image path is used in css background declarations */
            imgName: '../img/sprite.png',
            cssName: 'sprite.scss'
        }));
    spriteData.img.pipe(gulp.dest('img'));
    spriteData.css.pipe(gulp.dest('dev/sass/helpers'));
});

gulp.watch(['img/sprites/**/*.png'], ['sprite']);

gulp.task('watch', function () {
    gulp.watch(['img/sprites/**/*.png'], ['sprite']);
});
/* END Images Sprites */

gulp.task("default", ["plugins"]);
